import "./App.css"
import Header from "./components/header";
import Home from "./components/home"

const App = () => {
  return (
    <div>
      <Header /> 
      <Home /> 
    </div>
  )
}

export default App; 
 