import React from "react";
import Task from "./task";
import { useState } from "react";

const Home = () => {
  // usestate hooks
  const [tasks, setTasks] = useState([]);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");

  // function to handle on submit
  const submitHandler = (e) => {
    e.preventDefault();
    setTasks([...tasks, {
      title: title, 
      description: description
    }])
  };
  console.log(title, description) 

  // functions to get the targeted value from dom 
  const newTitle = (e) => setTitle(e.target.value) 
  const newDescription =(e) => setDescription(e.target.value)

  // component logic
  return (
    <div className="container">
      <h1>Daily tasks</h1>
      <form onSubmit={submitHandler}>
        <input
          type="text"
          placeholder="title"
          value={title}
          onChange={newTitle}
        />
        <textarea
          name=""
          id=""
          cols="30"
          rows="2"
          placeholder="description"
          value={description}
          onChange={ newDescription}
        ></textarea>
        <button type="submit">Add</button>
      </form>

      {tasks.map(() => (
        <Task />
      ))}
    </div>
  );
};

export default Home;
